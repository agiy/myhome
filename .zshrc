#brew
export PATH=/usr/local/bin:$PATH

#homestead
export PATH=~/.composer/vendor/bin:$PATH

#php7
export PATH="$(brew --prefix homebrew/php/php70)/bin:$PATH"

#zplug
##########################
export ZPLUG_HOME=/usr/local/opt/zplug
source $ZPLUG_HOME/init.zsh

#インストールされたファイルは/usr/local/Cellar/zplug/2.3.2/reposにある
zplug  'zsh-users/zsh-autosuggestions'
zplug  'zsh-users/zsh-completions'
zplug  'zsh-users/zsh-syntax-highlighting'
#zplug "zsh-users/zsh-history-substring-search"
zplug  'chrissicool/zsh-256color'

zplug "voronkovich/mysql.plugin.zsh"
zplug 'dracula/zsh', as:theme

# Support oh-my-zsh plugins and the like
  zplug "plugins/git",     from:oh-my-zsh, if:"which git"
  zplug "plugins/laravel5",     from:oh-my-zsh
  zplug "plugins/brew",    from:oh-my-zsh, if:"which brew"
#  zplug "themes/daveverwer", from:oh-my-zsh

if ! zplug check --verbose; then
  printf 'Install? [y/N]: '
  if read -q; then
    echo; zplug install
  fi
fi

zplug load --verbose
##############


#宮守定義
#############
# 文字コードの指定
export LANG=ja_JP.UTF-8

# 日本語ファイル名を表示可能にする
setopt print_eight_bit

# cdなしでディレクトリ移動
setopt auto_cd

# ビープ音の停止
setopt no_beep

# ビープ音の停止(補完時)
setopt nolistbeep

# cd -<tab>で以前移動したディレクトリを表示
setopt auto_pushd

# ヒストリ(履歴)を保存、数を増やす
HISTFILE=~/.zsh_history
HISTSIZE=100000
SAVEHIST=100000

# 同時に起動したzshの間でヒストリを共有する
setopt share_history

# 直前と同じコマンドの場合は履歴に追加しない
setopt hist_ignore_dups

# 同じコマンドをヒストリに残さない
setopt hist_ignore_all_dups

# スペースから始まるコマンド行はヒストリに残さない
setopt hist_ignore_space

# ヒストリに保存するときに余分なスペースを削除する
setopt hist_reduce_blanks

# キーバインディングをemacs風に(-vはvim)
 bindkey -v


# some more ls aliases
alias ll='exa -baghHliS'
alias la='ls -AG'
alias l='ls -CFG'
alias artisan='php artisan'

# grc color aliases
alias mount='grc mount'
alias ifconfig='grc ifconfig'
alias dig='grc dig'
alias ldap='grc ldap'
alias netstat='grc netstat'
alias ping='grc ping'
alias ps='grc ps'
alias traceroute='grc traceroute'
alias gcc='grc gcc'
alias diff='colordiff'


autoload -Uz chpwd_recent_dirs cdr add-zsh-hook
add-zsh-hook chpwd chpwd_recent_dirs
zstyle ':chpwd:*' recent-dirs-max 500 # cdrの履歴を保存する個数
zstyle ':chpwd:*' recent-dirs-default yes
zstyle ':completion:*' recent-dirs-insert both

zstyle ':filter-select:highlight' selected fg=black,bg=white,standout
zstyle ':filter-select' case-insensitive yes

bindkey '^@' zaw-cdr
bindkey '^T' zaw-history
bindkey '^X^F' zaw-git-files
bindkey '^X^B' zaw-git-branches
bindkey '^X^P' zaw-process
bindkey '^A'  beginning-of-line
bindkey '^E'  end-of-line
#bindkey '^R'  history-beginning-search-backward
bindkey '^R'  history-incremental-search-backward
#bindkey '^S'  history-beginning-search-forward
bindkey '^S'  history-incremental-search-forward


########################################
# キーバインド

# ^R で履歴検索をするときに * でワイルドカードを使用出来るようにする
#bindkey '^R' history-incremental-pattern-search-backward


########################################
# C で標準出力をクリップボードにコピーする
# mollifier delta blog : http://mollifier.hatenablog.com/entry/20100317/p1
if which pbcopy >/dev/null 2>&1 ; then
    # Mac
    alias -g C='| pbcopy'
elif which xsel >/dev/null 2>&1 ; then
    # Linux
    alias -g C='| xsel --input --clipboard'
elif which putclip >/dev/null 2>&1 ; then
    # Cygwin
    alias -g C='| putclip'
fi


#alias ssh=/Users/miyamorimusashi/Library/sh/ssh-host-color.sh

########################################

# vim:set ft=zsh:

#if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi

#export PATH=$PATH:/Users/miyamorimusashi/Library/Android/sdk/platform-tools

precmd() {
    echo -ne "\ek$(hostname|awk 'BEGIN{FS="."}{print $1}'):idle\e\\"
}
source /Users/miyamorimusashi/zaw/zaw.zsh


# zsh-bd
. $HOME/.zsh/plugins/bd/bd.zsh

export PATH=$HOME/.nodebrew/current/bin:$PATH

#LS_COLORSを設定しておく
export LS_COLORS='di=34:ln=35:so=32:pi=33:ex=31:bd=46;34:cd=43;34:su=41;30:sg=46;30:tw=42;30:ow=43;30'
#ファイル補完候補に色を付ける
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
