#!/bin/sh

cli=/Applications/Karabiner.app/Contents/Library/bin/karabiner

$cli set private.vim_keybind_apps_esc_with_eisuu 1
/bin/echo -n .
$cli set remap.jis_backquote_kana_eisuu 1
/bin/echo -n .
$cli set remap.mouse_button4_5 1
/bin/echo -n .
$cli set remap.pclikehomeend 1
/bin/echo -n .
$cli set repeat.initial_wait 250
/bin/echo -n .
$cli set repeat.wait 33
/bin/echo -n .
/bin/echo
